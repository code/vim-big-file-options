" If we can use filesize to detect the big file early, we should
function! big_file_options#() abort

  " Try and get filesize; if we can't, defer another check attempt until after
  " everything's been loaded
  let size = getfsize(bufname('%'))
  if size == -1
    autocmd big_file_options BufReadPost,StdinReadPost <buffer>
          \ call s:CheckPost()
          \|autocmd! big_file_options BufReadPost,StdinReadPost <buffer>
    return
  endif

  " Set the buffer's big flag to whether the file is verifiably outsize
  let b:big_file_options_big = size > s:Limit() || size == -2

  " If we found it's a big file, call the early options set
  if b:big_file_options_big
    call s:SetPre()
    autocmd big_file_options BufReadPost,StdinReadPost <buffer>
          \ call s:SetPost()
          \|autocmd! big_file_options BufReadPost,StdinReadPost <buffer>
  endif

endfunction

" If it's still indeterminate (stdin read?), try to check the buffer size
" itself
function! s:CheckPost() abort

  " Test buffer size, bail if that doesn't work either
  let size = line2byte(line('$') + 1)
  if size == -1
    return
  endif

  " Flag the buffer's oversize status; if it's positive, we'll catch up and
  " run the early options set now
  let b:big_file_options_big = size > s:Limit()
  if b:big_file_options_big
    call s:SetPre()
    call s:SetPost()
  endif

endfunction

" Wrapper function to get the configured size limit, default to 10 MiB
function! s:Limit() abort
  let limit = get(g:, 'big_file_options_limit', 10 * 1024 * 1024)
  return limit
endfunction

" These options can and should be set as early as possible
function! s:SetPre() abort

  " These are always set
  setlocal noswapfile
  setlocal undolevels=-1
  if has('persistent_undo')
    setlocal noundofile
  endif

  " Decide whether to set readonly options
  let readonly = get(g:, 'big_file_options_readonly', 1)
  if readonly
    setlocal buftype=nowrite
    setlocal nomodifiable
    setlocal readonly
  endif

endfunction

" These options need to be set later, after the buffer has loaded
function! s:SetPost() abort

  " Force filetype off
  setlocal filetype=

  " Disable syntax highlighting if configured
  let syntax = get(g:, 'big_file_options_syntax', 0)
  if !syntax
    setlocal syntax=OFF
  endif

  " Force maximum syntax columns down if configured
  let synmaxcol = get(g:, 'big_file_options_synmaxcol', 256)
  if &synmaxcol > synmaxcol
    let &l:synmaxcol = synmaxcol
  endif

  " Tell the user what we've done
  echomsg 'Big file detected, set appropriate options'

endfunction
