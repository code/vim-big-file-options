"
" big_file_options.vim: When opening a large file, take some measures to keep
" things loading quickly.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_big_file_options') || &compatible || v:version < 700
  finish
endif
let loaded_big_file_options = 1

" Define autocmd for calling to check filesize
augroup big_file_options
  autocmd!
  autocmd BufReadPre,StdinReadPre *
        \ call big_file_options#()
augroup end
