big\_file\_options.vim
======================

This plugin adds an `autocmd` hook to check the file size of an incoming
buffer, and if it's over a certain threshold, disables certain options in order
to make the file a bit easier to edit.  It makes the buffer read-only, and
disables filetypes, swap files, undo files, and syntax highlighting.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
